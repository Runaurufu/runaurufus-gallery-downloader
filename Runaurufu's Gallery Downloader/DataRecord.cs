﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using Runaurufu.Data.Connector;
using Runaurufu.Data.Structure;
using Runaurufu.GalleryDownloader.Base;
using Runaurufu.Utility.Reflection;

namespace Runaurufu.GalleryDownloader
{
  public class RecordData : IRecordData
  {
    private AbstractConnector connector = null;

    public void CreateDatabase(String path)
    {
      SQLiteConnector.CreateDatabase(path);

      SQLiteConnector connector = new SQLiteConnector("Version=3;Data Source=" + path + ";");

      SQLiteStructure structure = new SQLiteStructure(connector);
      structure.CreateTable(new TableStructure("provider")
      {
        Columns =
        {
          {"id" , new RowStructure(null, "id", new GenericDataStructure("INTEGER", false))
            {   IsPrimaryKey = true }
          },
          {"name" , new RowStructure(null, "name", new GenericDataStructure("TEXT", false))}
        }
      });

      structure.CreateTable(new TableStructure("providerSettings")
      {
        Columns =
        {
          {"providerId", new RowStructure(null, "providerId", new GenericDataStructure("INTEGER", false))},
          {"key" , new RowStructure(null, "key", new GenericDataStructure("TEXT", false))},
          {"value" , new RowStructure(null, "value", new GenericDataStructure("TEXT", false))},
        }
      });

      structure.CreateTable(new TableStructure("gallery")
      {
        Columns =
        {
          {"id" , new RowStructure(null, "id", new GenericDataStructure("INTEGER", false))
            {   IsPrimaryKey = true }
          },
          {"providerId", new RowStructure(null, "providerId", new GenericDataStructure("INTEGER", false))},
          {"name" , new RowStructure(null, "name", new GenericDataStructure("TEXT", false))},
          {"uri" , new RowStructure(null, "uri", new GenericDataStructure("TEXT", false))},
          {"savePath" , new RowStructure(null, "savePath", new GenericDataStructure("TEXT", false))},
        }
      });

      structure.CreateTable(new TableStructure("fetchedItem")
      {
        Columns =
        {
          {"galleryId" , new RowStructure(null, "galleryId", new GenericDataStructure("INTEGER", false))},
          {"ident" , new RowStructure(null, "ident", new GenericDataStructure("TEXT", false))},
        }
      });

    }

    public void Init()
    {
      // init providers!
      Provider[] currentProviders = this.FetchProviders();

      Type[] providerTypes = ClassManager.GetTypesImplementingInterface(typeof(IProviderDefinition));
      foreach (Type providerType in providerTypes)
      {
        IProviderDefinition def = (IProviderDefinition)Activator.CreateInstance(providerType);

        if(currentProviders.Any(p => p.Name == def.Name) == false)
        {
          Provider newProvider = new Provider();
          newProvider.Name = def.Name;

          this.RegisterProvider(newProvider);
        }
      }
    }

    public void SaveProviderSettings(Provider provider)
    {
      Dictionary<string, string> dict = provider.AssociatedProvider.GetSettings();

      this.connector.ExecuteNonQuery("DELETE FROM [providerSettings] WHERE [providerId] = ?", provider.Id);

      foreach (KeyValuePair<string, string> pair in dict)
      {
        this.connector.Insert("providerSettings",
        new Dictionary<string, object>()
        {
          {"providerId", provider.Id},
          {"key", pair.Key ?? string.Empty},
          {"value", pair.Value ?? string.Empty},
        });
      }
    }

    private void LoadProviderSettings(Provider provider)
    {
      Dictionary<string, string> settings = new Dictionary<string,string>();
      using(DataTable dt = this.connector.ExecuteQuery("SELECT [key], [value] FROM [providerSettings] WHERE [providerId] = ?", provider.Id))
      {
        foreach (DataRow row in dt.Rows)
        {
          string key = (string)row["key"];
          string value = (string)row["value"];
          settings.Add(key, value);
        }
      }
      provider.AssociatedProvider.SetSettings(settings);
    }

    public void Open(String path)
    {
      this.Close();
      this.connector = new SQLiteConnector("Version=3;Data Source=" + path + ";");
    }

    public void Close()
    {
      if (this.connector == null)
        return;
      this.connector.Dispose();
      this.connector = null;
    }

    public bool WasItemDownloaded(Gallery gallery, string ident)
    {
      object obj = this.connector.ExecuteScalar("SELECT COUNT(*) FROM [fetchedItem] WHERE [galleryId] = ? AND [ident] = ?", gallery.Id, ident);
      Int64 howMuch = Convert.ToInt64(obj);
      return howMuch > 0;
    }

    public Provider RegisterProvider(Provider provider)
    {
      provider.Id = this.connector.Insert("provider",
        new Dictionary<string, object>()
        {
          {"name", provider.Name},
        });
      return provider;
    }

    public Gallery RegisterGallery(Provider provider, Gallery gallery)
    {
      gallery.Id = this.connector.Insert("gallery",
        new Dictionary<string,object>()
        {
          {"providerId", provider.Id},
          {"name", gallery.Name ?? string.Empty},
          {"uri", gallery.Uri ?? string.Empty},
          {"savePath", gallery.SavePath ?? string.Empty},
        });
      return gallery;
    }

    public void UpdateGallery(Gallery gallery)
    {
      this.connector.Update("gallery",
        new Dictionary<string, object>()
        {
          {"name", gallery.Name ?? string.Empty},
          {"uri", gallery.Uri ?? string.Empty},
          {"savePath", gallery.SavePath ?? string.Empty},
        },
        new Dictionary<string, object>()
        {
          {"id", gallery.Id},
        });
    }

    public void UnRegisterGallery(Gallery gallery)
    {
      this.connector.ExecuteNonQuery("DELETE FROM [fetchedItem] WHERE [galleryId] = ?", gallery.Id);
      this.connector.ExecuteNonQuery("DELETE FROM [gallery] WHERE [id] = ?", gallery.Id);
    }

    public void RegisterDownloadedItem(Gallery gallery, string itemIdent)
    {
      this.connector.Insert("fetchedItem",
        new Dictionary<string, object>()
        {
          {"galleryId", gallery.Id},
          {"ident", itemIdent},
        });
    }

    public Provider[] FetchProviders()
    {
      List<Provider> providers = new List<Provider>();
      using(DataTable dt = this.connector.ExecuteQuery("SELECT [id], [name] FROM [provider]"))
      {
        foreach (DataRow row in dt.Rows)
        {
          Provider provider = new Provider();
          provider.Id = (Int64) row["id"];
          provider.Name = (String) row["name"];

          Gallery[] galleries = FetchGalleries(provider);
          provider.SetGalleries(galleries);

          LoadProviderSettings(provider);

          providers.Add(provider);
        }
      }
      return providers.ToArray();
    }

    public Gallery[] FetchGalleries(Provider provider)
    {
      List<Gallery> galleries = new List<Gallery>();
      using (DataTable dt = this.connector.ExecuteQuery("SELECT [id], [providerId], [name], [uri], [savePath] FROM [gallery] WHERE [providerId] = ?", provider.Id))
      {
        foreach (DataRow row in dt.Rows)
        {
          Gallery gallery = new Gallery();
          gallery.Id = (Int64)row["id"];
        //  gallery.ProviderId = (Int64)row["providerId"];
          gallery.Name = (String)row["name"];
          gallery.Uri = (String)row["uri"];
          gallery.SavePath = (String)row["savePath"];

          galleries.Add(gallery);
        }
      }
      return galleries.ToArray();
    }
  }

  public class Provider : INotifyPropertyChanged
  {
    private Int64 id;
    public Int64 Id
    {
      get { return this.id; }
      set
      {
        this.id = value;
        NotifyPropertyChanged("Id");
      }
    }

    private String name;
    public String Name
    {
      get { return this.name; }
      set
      {
        this.name = value;
        NotifyPropertyChanged("Name");
      }
    }

    List<Gallery> galleries = new List<Gallery>();
    public List<Gallery> Galleries
    {
      get { return this.galleries; }
    }

    public void AddGallery(Gallery gallery)
    {
      if(this.galleries.Contains(gallery) == false)
      {
        if(gallery.Id == 0)
          App.DataRecord.RegisterGallery(this, gallery);

        this.galleries.Add(gallery);
        NotifyPropertyChanged("Galleries");
      }
    }

    public void SetGalleries(Gallery[] galleries)
    {
      this.galleries.Clear();
      this.galleries.AddRange(galleries);
      NotifyPropertyChanged("Galleries");
    }

    public void RemoveGallery(Gallery gallery)
    {
      if(this.galleries.Contains(gallery))
      {
        if (gallery.Id != 0)
          App.DataRecord.UnRegisterGallery(gallery);

        this.galleries.Remove(gallery);
        NotifyPropertyChanged("Galleries");
      }
    }

    private IProviderDefinition associatedProvider = null;
    public IProviderDefinition AssociatedProvider
    {
      get
      {
        if(this.associatedProvider == null)
        {
          Type[] types = ClassManager.GetTypesImplementingInterface(typeof(IProviderDefinition));
          foreach (Type t in types)
          {
            if (t.IsClass || t.IsValueType)
            {
              IProviderDefinition def = Activator.CreateInstance(t) as IProviderDefinition;
              if (def.Name == this.Name)
              {
                this.associatedProvider = def;
                break;
              }
            }
          }
        }
        return this.associatedProvider;
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    void NotifyPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }

  
}
