﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Runaurufu.GalleryDownloader.Base;
using Runaurufu.Utility;

namespace Runaurufu.GalleryDownloader
{
  /// <summary>
  /// Interaction logic for ProgressWindow.xaml
  /// </summary>
  public partial class ProgressWindow : Window, IProgressHandling
  {
    public ProgressWindow()
    {
      InitializeComponent();
    }

    public ThreadWorker Worker { get; set; }

    private void ProgressBarTextSet(ProgressBar bar, TextBlock text)
    {
      if (bar.IsIndeterminate == false)
      {
        text.Text = bar.Value + " / " + bar.Maximum;
      }
    }

    public void SetProviderSteps(int steps)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProviderProgressBar.IsIndeterminate = false;
        this.ProviderProgressBar.Minimum = 0;
        this.ProviderProgressBar.Maximum = steps;
        this.ProviderProgressBar.Value = 0;

        ProgressBarTextSet(this.ProviderProgressBar, this.ProviderProgressText);
      }));
    }

    public void NoticeProviderIncrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProviderProgressBar.Value += progress;

        ProgressBarTextSet(this.ProviderProgressBar, this.ProviderProgressText);
      }));
    }

    public void NoticeProviderDecrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProviderProgressBar.Value -= progress;

        ProgressBarTextSet(this.ProviderProgressBar, this.ProviderProgressText);
      }));
    }

    public void SetGallerySteps(int steps)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.GalleryProgressBar.IsIndeterminate = false;
        this.GalleryProgressBar.Minimum = 0;
        this.GalleryProgressBar.Maximum = steps;
        this.GalleryProgressBar.Value = 0;

        ProgressBarTextSet(this.GalleryProgressBar, this.GalleryProgressText);
      }));
    }

    public void NoticeGalleryIncrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.GalleryProgressBar.Value += progress;

        ProgressBarTextSet(this.GalleryProgressBar, this.GalleryProgressText);
      }));
    }

    public void NoticeGalleryDecrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.GalleryProgressBar.Value -= progress;

        ProgressBarTextSet(this.GalleryProgressBar, this.GalleryProgressText);
      }));
    }

    public void SetProcessingSteps(int steps)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProcessingProgressBar.IsIndeterminate = false;
        this.ProcessingProgressBar.Minimum = 0;
        this.ProcessingProgressBar.Maximum = steps;
        this.ProcessingProgressBar.Value = 0;

        ProgressBarTextSet(this.ProcessingProgressBar, this.ProcessingProgressText);
      }));
    }

    public void SetProcessingContinuous(string progressText)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProcessingProgressBar.IsIndeterminate = true;
        this.ProcessingProgressText.Text = progressText;
      }));
    }

    public void NoticeProcessingIncrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProcessingProgressBar.Value += progress;

        ProgressBarTextSet(this.ProcessingProgressBar, this.ProcessingProgressText);
      }));
    }

    public void NoticeProcessingDecrease(int progress = 1)
    {
      this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
      {
        this.ProcessingProgressBar.Value -= progress;

        ProgressBarTextSet(this.ProcessingProgressBar, this.ProcessingProgressText);
      }));
    }

    private void AbortButton_Click(object sender, RoutedEventArgs e)
    {
      this.AbortButton.IsEnabled = false;
      this.Worker.Stop();
    }

    public bool IsStopRequested
    {
      get
      {
        if (this.Worker == null)
          return false;
        return this.Worker.IsStopRequested;
      }
    }
  }
}
