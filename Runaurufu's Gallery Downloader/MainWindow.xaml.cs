﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Runaurufu.GalleryDownloader.Base;
using Runaurufu.Utility;

namespace Runaurufu.GalleryDownloader
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window, INotifyPropertyChanged
  {
    private ThreadWorker downloadingWorker = new ThreadWorker(ParameterizedThreadWorkerStart);

    public MainWindow()
    {
      this.DataContext = this;
      InitializeComponent();

      this.Loaded += MainWindow_Loaded;
      this.Closing += MainWindow_Closing;

      this.downloadingWorker.ThreadWorkerStopped += downloadingWorker_ThreadWorkerStopped;
    }

    void MainWindow_Closing(object sender, CancelEventArgs e)
    {
      if (this.providers != null)
      {
        foreach (Provider provider in this.providers)
        {
          App.DataRecord.SaveProviderSettings(provider);
        }
      }
    }

    void MainWindow_Loaded(object sender, RoutedEventArgs e)
    {
      this.Providers = App.DataRecord.FetchProviders();
    }

    private Provider[] providers;
    public Provider[] Providers
    {
      get { return this.providers; }
      set
      {
        this.providers = value;
        NotifyPropertyChanged("Providers");
      }
    }

    public Gallery[] Galleries
    {
      get
      {
        if (this.currentProvider == null)
          return new Gallery[0];
        return this.currentProvider.Galleries.ToArray();
      }
    }

    private Provider currentProvider;
    public Provider CurrentProvider
    {
      get { return this.currentProvider; }
      set
      {
        if (this.currentProvider == value)
          return;

        this.currentProvider = value;
        NotifyPropertyChanged("CurrentProvider");
        NotifyPropertyChanged("Galleries");

        this.providerConfigurationParent.Children.Clear();

        if (this.currentProvider.AssociatedProvider == null)
          return;
        IProviderSettingsControl providerSettings = this.currentProvider.AssociatedProvider.GetSettingsControl();

        if (providerSettings != null && providerSettings is UIElement)
        {
          providerSettings.Provider = this.currentProvider.AssociatedProvider;
          providerSettings.SetSettingsToControl();
          this.providerConfigurationParent.Children.Add((UIElement)providerSettings);
        }
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    void NotifyPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
      }
    }

    private void ProvidersList_Click(object sender, RoutedEventArgs e)
    {
      Button button = sender as Button;
      Provider selectedProvider = button.DataContext as Provider;

      this.CurrentProvider = selectedProvider;
    }

    private void buttonAddGallery_Click(object sender, RoutedEventArgs e)
    {
      if (this.currentProvider == null)
        return;

      GalleryWindow window = new GalleryWindow();

      Gallery newGallery = new Gallery();
      window.Gallery = newGallery;

      window.Owner = this;
      if (window.ShowDialog() == true)
      {
        this.currentProvider.AddGallery(newGallery);
        NotifyPropertyChanged("Galleries");
      }
    }

    private void buttonEditGallerySavePath_Click(object sender, RoutedEventArgs e)
    {
      if (this.currentProvider == null)
        return;

      List<string> savePathes = new List<string>();
      foreach (Gallery gallery in this.currentProvider.Galleries)
      {
        if (savePathes.Contains(gallery.SavePath))
          continue;

        string galleryTop = System.IO.Path.GetDirectoryName(gallery.SavePath);
        if (savePathes.Contains(galleryTop))
          continue;

        bool providerGalleriesContinue = false;
        for (int i = 0; i < savePathes.Count; i++)
        {
          string savedGalleryTop = System.IO.Path.GetDirectoryName(savePathes[i]);
          if (savedGalleryTop == galleryTop)
          {
            savePathes.RemoveAt(i);
            i--;

            savePathes.Add(galleryTop);
            providerGalleriesContinue = true;
            break;
          }
        }

        if (providerGalleriesContinue)
          continue;

        savePathes.Add(gallery.SavePath);
      }

      foreach (string path in savePathes)
      {
        using (System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog())
        {
          fbd.SelectedPath = path;
          fbd.ShowNewFolderButton = true;
          fbd.Description = "Select new root path for galleries saved in " + path;

          if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
          {
            if (fbd.SelectedPath == path)
              continue;

            foreach (Gallery gallery in this.currentProvider.Galleries)
            {
              if (gallery.SavePath.StartsWith(path))
              {
                string newPath = gallery.SavePath.Replace(path, fbd.SelectedPath);
                gallery.SavePath = newPath;
                App.DataRecord.UpdateGallery(gallery);
              }
            }
          }
        }
      }
    }

    private void buttonEditGallery_Click(object sender, RoutedEventArgs e)
    {
      if (this.currentProvider == null)
        return;

      Button button = sender as Button;
      Gallery gallery = button.DataContext as Gallery;
      if (gallery == null)
        return;

      GalleryWindow window = new GalleryWindow();
      window.Gallery = gallery;

      window.Owner = this;
      if (window.ShowDialog() == true)
      {
        App.DataRecord.UpdateGallery(gallery);
        NotifyPropertyChanged("Galleries");
      }
    }

    private void buttonRemoveGallery_Click(object sender, RoutedEventArgs e)
    {
      if (this.currentProvider == null)
        return;

      Button button = sender as Button;
      Gallery gallery = button.DataContext as Gallery;
      if (gallery == null)
        return;

      this.CurrentProvider.RemoveGallery(gallery);
      NotifyPropertyChanged("Galleries");
    }

    private void DownloadButton_Click(object sender, RoutedEventArgs e)
    {
      ProgressWindow progress = new ProgressWindow();
      progress.Owner = this;
      progress.Worker = this.downloadingWorker;

      this.downloadingWorker.Stop();
      this.downloadingWorker.Start(new Tuple<Provider[], ProgressWindow>(this.Providers, progress));

      this.Hide();

      progress.Closing += progress_Closing;
      progress.ShowDialog();
    }

    void progress_Closing(object sender, CancelEventArgs e)
    {
      e.Cancel = true;
    }

    void downloadingWorker_ThreadWorkerStopped(ThreadWorker worker, object obj = null)
    {
      if (obj is Tuple<Provider[], ProgressWindow>)
      {
        Tuple<Provider[], ProgressWindow> tuple = (Tuple<Provider[], ProgressWindow>)obj;
        this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
        {
          this.Show();

          tuple.Item2.Closing -= progress_Closing;
          tuple.Item2.Close();
        }));
      }
      else if (obj is Tuple<Provider, ProgressWindow>)
      {
        Tuple<Provider, ProgressWindow> tuple = (Tuple<Provider, ProgressWindow>)obj;
        this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)(() =>
        {
          this.Show();

          tuple.Item2.Closing -= progress_Closing;
          tuple.Item2.Close();
        }));
      }
    }

    private static void ParameterizedThreadWorkerStart(ThreadWorkerState workerState, object obj)
    {
      if (obj is Tuple<Provider[], ProgressWindow>)
      {
        Tuple<Provider[], ProgressWindow> tuple = (Tuple<Provider[], ProgressWindow>)obj;

        tuple.Item2.SetProviderSteps(tuple.Item1.Length);
        foreach (Provider provider in tuple.Item1)
        {
          if (workerState.IsStopRequested)
            break;


          ProviderGalleriesDownload(provider, tuple.Item2);

          tuple.Item2.NoticeProviderIncrease(1);
        }
      }
      else if (obj is Tuple<Provider, ProgressWindow>)
      {
        Tuple<Provider, ProgressWindow> tuple = (Tuple<Provider, ProgressWindow>)obj;

        tuple.Item2.SetProviderSteps(1);
        ProviderGalleriesDownload(tuple.Item1, tuple.Item2);
        tuple.Item2.NoticeProviderIncrease(1);
      }
    }

    private static void ProviderGalleriesDownload(Provider provider, ProgressWindow progress)
    {
      progress.SetGallerySteps(provider.Galleries.Count);

      provider.AssociatedProvider.PreGalleryItemsDownload(progress);
      try
      {

        for (int i = 0; i < provider.Galleries.Count; i++)
        {
          if (progress.IsStopRequested)
            break;

          provider.AssociatedProvider.DownloadGalleryItems(progress, App.DataRecord, provider.Galleries[i]);

          progress.NoticeGalleryIncrease(1);
        }
      }
      finally
      {
        provider.AssociatedProvider.PostGalleryItemsDownload(progress);
      }
    }
  }
}
