﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Runaurufu.GalleryDownloader.Base;

namespace Runaurufu.GalleryDownloader
{
  /// <summary>
  /// Interaction logic for GalleryWindow.xaml
  /// </summary>
  public partial class GalleryWindow : Window
  {
    public GalleryWindow()
    {
      InitializeComponent();

      this.Loaded += GalleryWindow_Loaded;
    }

    void GalleryWindow_Loaded(object sender, RoutedEventArgs e)
    {
      this.galleryName.Text = this.Gallery.Name;
      this.galleryUri.Text = this.Gallery.Uri;
      this.gallerySavePath.Text = this.Gallery.SavePath;
    }

    public Gallery Gallery { get; set; }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      this.Gallery.Name = this.galleryName.Text;
      this.Gallery.Uri = this.galleryUri.Text;
      this.Gallery.SavePath = this.gallerySavePath.Text;
      this.DialogResult = true;
    }

    private void BrowseButton_Click(object sender, RoutedEventArgs e)
    {
      using(System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog())
      {
        fbd.SelectedPath = this.gallerySavePath.Text;

        if(fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          this.gallerySavePath.Text = fbd.SelectedPath;
        }
      }
    }
  }
}
