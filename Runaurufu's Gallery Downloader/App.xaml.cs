﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using Runaurufu.Utility.Reflection;

namespace Runaurufu.GalleryDownloader
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    readonly string SettingFilePath = Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "downloadRecord.set");

    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);

      // we startuped?

      AssemblyManager.BindApplicationDomain();

      AssemblyManager.AddAssemblyDirectory(Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "Plugins"));
      AssemblyManager.LoadAllAssemblies(false);

      ClassManager.Rebuild();
      TypeAssociationManager.Rebuild();

      RecordData record = new RecordData();

      if(File.Exists(SettingFilePath) == false)
      {
        record.CreateDatabase(SettingFilePath);
      }

      record.Open(SettingFilePath);

      // init database with data from assemblies and so on
      record.Init();

      this.dataRecord = record;
    }

    protected override void OnExit(ExitEventArgs e)
    {
      if (this.dataRecord != null)
        this.dataRecord.Close();

      base.OnExit(e);
    }

    public static RecordData DataRecord
    {
      get
      {
        return ((App)App.Current).dataRecord;
      }
    }

    private RecordData dataRecord;
  }
}
