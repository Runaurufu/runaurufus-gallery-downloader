﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Runaurufu.Extension;
using Runaurufu.GalleryDownloader.Base;

namespace Runaurufu.GalleryDownloader.Plugins
{
  public class DevianArtProvider : IProviderDefinition
  {
    public const String SettingUserName = "authUserName";
    public const String SettingUserPassword = "authUserPassword";

    private readonly static int MaxDownloadRetryAttempts = 10;

    public string Name
    {
      get
      {
        return "deviantart.com";
      }
    }

    public string AuthUserName { get; set; }
    public string AuthUserPassword { get; set; }

    private CookieAwareWebClient client = null;

    public void SetSettings(Dictionary<string, string> settings)
    {
      if (settings.ContainsKey(SettingUserName))
        this.AuthUserName = settings[SettingUserName];
      if (settings.ContainsKey(SettingUserPassword))
        this.AuthUserPassword = settings[SettingUserPassword];
    }

    public Dictionary<string, string> GetSettings()
    {
      Dictionary<string, string> settings = new Dictionary<string, string>();

      settings[SettingUserName] = this.AuthUserName;
      settings[SettingUserPassword] = this.AuthUserPassword;

      return settings;
    }

    public IProviderSettingsControl GetSettingsControl()
    {
      return new DevianArtSettings();
    }

    public bool PreGalleryItemsDownload(IProgressHandling progress)
    {
      if (this.client != null)
      {
        this.client.Dispose();
        this.client = null;
      }

      client = new CookieAwareWebClient();
      client.MinimalInterval = TimeSpan.FromSeconds(0.5);
      client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
      client.PersistentHeaders[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0";
      //"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";

      string downloadedPage = null;

      bool repeat = true;
      do
      {
        int attempts = 0;
        do
        {
          if (progress.IsStopRequested)
            return false;

          try
          {
            downloadedPage = client.DownloadString("https://www.deviantart.com/users/login");
            repeat = false;
          }
          catch (WebException)
          {
            attempts++;
            if (attempts >= MaxDownloadRetryAttempts)
              return false;
          }
        } while (repeat);

        string searchFor = "<input type=\"hidden\" name=\"validate_token\" value=\"";
        int index = downloadedPage.IndexOf(searchFor) + searchFor.Length;
        int nextIndex = downloadedPage.IndexOf('"', index);
        string validateToken = downloadedPage.Substring(index, nextIndex - index);

        searchFor = "<input type=\"hidden\" name=\"validate_key\" value=\"";
        index = downloadedPage.IndexOf(searchFor) + searchFor.Length;
        nextIndex = downloadedPage.IndexOf('"', index);
        string validateKey = downloadedPage.Substring(index, nextIndex - index);

        NameValueCollection loginValues = new NameValueCollection
        {
          { "username", this.AuthUserName },
          { "password", this.AuthUserPassword },
          { "remember_me", "1" },
          { "validate_token", validateToken},
          { "validate_key", validateKey}
        };

        repeat = true;

        if (progress.IsStopRequested)
          return false;

        try
        {
          client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
          byte[] resp = client.UploadValues("https://www.deviantart.com/users/login", loginValues);

          downloadedPage = System.Text.Encoding.UTF8.GetString(resp);
          repeat = false;
        }
        catch (WebException)
        {
          attempts++;
          if (attempts >= MaxDownloadRetryAttempts * 2)
            return false;
        }
      } while (repeat);

      return true;
    }

    public void PostGalleryItemsDownload(IProgressHandling progress)
    {
      try
      {
        if (this.client != null)
        {
          // Log out to prevent stockpiling sessions!
          client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
          //loginValues.Clear();
          //byte[] resp = client.UploadValues("https://www.deviantart.com/users/logout", loginValues);

          //downloadedPage = System.Text.Encoding.UTF8.GetString(resp);




          this.client.Dispose();
          this.client = null;
        }
      }
      catch (Exception ex)
      {

      }
    }

    public void DownloadGalleryItems(IProgressHandling progress, IRecordData record, Gallery gallery)
    {
      progress.SetProcessingContinuous("Acquiring gallery data");

      List<string> galleryItems = new List<string>();

      bool repeat = true;

      string downloadedPage = null;

      repeat = true;
      int attempts = 0;

      // Fetch gallery
      int offset = 0;

      Regex regex = new Regex(@"""http:\/\/([A-Za-z0-9]|\-)+\.deviantart\.com\/art\/[^#]*?""");
      bool nextLoop = true;
      do
      {
        string address = gallery.Uri;
        if (address.Contains('?'))
          address += "&";
        else
          address += "?";
        address += "offset=" + offset.ToString();

        repeat = true;
        bool breakDoWhile = false;
        attempts = 0;

        DateTime startDateTime = DateTime.Now;
        do
        {
          if (progress.IsStopRequested)
            return;



          try
          {
            downloadedPage = client.DownloadString(address);
            repeat = false;
          }
          catch (WebException wex)
          {
            attempts++;
            HttpWebResponse response = wex.Response as HttpWebResponse;
            if (response != null && response.StatusCode == HttpStatusCode.NotFound)
            {
              return; // account was not found or was deactivated
            }

            if (wex.Status == WebExceptionStatus.ConnectionClosed && (DateTime.Now - startDateTime).TotalSeconds > 60)
            {
              return; // account was not found or was deactivated
            }

            if (attempts >= MaxDownloadRetryAttempts)
            {
              breakDoWhile = true;
              break;
            }
          }
        } while (repeat);

        if (breakDoWhile)
          break;

        int startIndex = downloadedPage.IndexOf(" id=\"gmi-ResourceStream\"");
        int endIndex = downloadedPage.IndexOf(" id=\"gallery_pager\"");

        if (startIndex != -1 && endIndex != -1)
        {
          string itemsContainer = downloadedPage.Substring(startIndex, endIndex - startIndex);

          foreach (Match match in regex.Matches(itemsContainer))
          {
            string foundUrl = match.Value.Substring(1, match.Value.Length - 2);
            if (galleryItems.Contains(foundUrl) == false)
              galleryItems.Add(foundUrl);
          }
        }

        offset += 24;

        int nextA = downloadedPage.IndexOf("<li class=\"next\">");
        int nextB = downloadedPage.IndexOf("<a class=\"disabled\">", nextA);
        int nextC = downloadedPage.IndexOf("<span>Next</span>", nextA);

        if (nextA < nextB && nextB < nextC)
          nextLoop = false;

        if (progress.IsStopRequested)
          return;

      } while (nextLoop);

      // Fetch each gallery item

      regex = new Regex(@"""http:\/\/www\.deviantart\.com\/download\/[0-9]*\/.*?""");
      Regex contentDownload = new Regex(@"<img collect_rid=""[0-9:]*"" src=""http:\/\/[\w\d\-]*\.deviantart\.net\/[\s\S]*?>");
      Regex contentUrlDownload = new Regex(@"http:\/\/[\w\d\-]*\.deviantart\.net\/[^""]*");

      string[] bestClasses = { @"""dev-content-full""", @"""dev-content-normal""" };

      progress.SetProcessingSteps(galleryItems.Count);

      bool checkDirectory = true;
      int index;

      foreach (string galleryItem in galleryItems)
      {
        index = galleryItem.LastIndexOf('-');
        string ident = galleryItem.Substring(index + 1);

        if (record.WasItemDownloaded(gallery, ident))
        {
          progress.NoticeProcessingIncrease(1);
          continue;
        }

        // create directory only if we are really going to download anything
        if (checkDirectory && Directory.Exists(gallery.SavePath) == false)
          Directory.CreateDirectory(gallery.SavePath);

        checkDirectory = false;

        // DOWNLOAD!
        repeat = true;
        bool continueOnGalleryItemsForeach = false;
        attempts = 0;
        do
        {
          if (progress.IsStopRequested)
            return;

          try
          {
            downloadedPage = client.DownloadString(galleryItem);
            repeat = false;
          }
          catch (WebException)
          {
            attempts++;

            if (attempts >= MaxDownloadRetryAttempts)
            {
              continueOnGalleryItemsForeach = true;
              break;
            }
          }
        } while (repeat);

        if (continueOnGalleryItemsForeach)
          continue;

        Match match = regex.Match(downloadedPage);
        if (match.Success)
        {
          Uri uri = new Uri(System.Net.WebUtility.HtmlDecode(match.Value.Substring(1, match.Length - 2)));
          String fileName = Path.GetFileName(uri.AbsolutePath);
          if (string.IsNullOrWhiteSpace(fileName))
            fileName = uri.Segments.Last().Replace(Path.GetInvalidFileNameChars(), '_');

          fileName = Path.Combine(gallery.SavePath, fileName);

          repeat = true;
          attempts = 0;

          do
          {
            if (progress.IsStopRequested)
              return;

            try
            {
              client.DownloadFile(uri, fileName);
              repeat = false;
            }
            catch (WebException wex)
            {
              attempts++;

              HttpWebResponse response = wex.Response as HttpWebResponse;
              if (response != null && response.StatusCode == HttpStatusCode.NotFound)
              {
                continueOnGalleryItemsForeach = true;
                break;
              }
            }

            if (attempts >= MaxDownloadRetryAttempts)
            {
              continueOnGalleryItemsForeach = true;
              break;
            }

          } while (repeat);

          if (continueOnGalleryItemsForeach)
          {
            progress.NoticeProcessingIncrease(1);
            continue;
          }

          record.RegisterDownloadedItem(gallery, ident);
        }
        else
        {
          MatchCollection matches = contentDownload.Matches(downloadedPage);

          if (matches.Count > 0)
          {
            string address = null;

            foreach (string bestClass in bestClasses)
            {
              foreach (Match matchItem in matches)
              {
                if (matchItem.Value.Contains(bestClass))
                {
                  // DOWNLOAD!
                  match = contentUrlDownload.Match(matchItem.Value);
                  address = match.Value;
                  break;
                }
              }
              if (address != null)
                break;
            }

            if (address == null)
            {
              match = contentUrlDownload.Match(matches[matches.Count - 1].Value);
              address = match.Value;
            }

            if (address != null)
            {
              String fileName = Path.Combine(gallery.SavePath, Path.GetFileName(address));

              repeat = true;
              attempts = 0;
              do
              {
                if (progress.IsStopRequested)
                  return;

                try
                {
                  client.DownloadFile(address, fileName);
                  repeat = false;
                }
                catch (WebException wex)
                {
                  attempts++;
                  HttpWebResponse response = wex.Response as HttpWebResponse;
                  if (response != null && response.StatusCode == HttpStatusCode.NotFound)
                  {
                    continueOnGalleryItemsForeach = true;
                    break;
                  }

                  if (attempts >= MaxDownloadRetryAttempts)
                  {
                    continueOnGalleryItemsForeach = true;
                    break;
                  }
                }
              } while (repeat);

              if (continueOnGalleryItemsForeach)
              {
                progress.NoticeProcessingIncrease(1);
                continue;
              }

              record.RegisterDownloadedItem(gallery, ident);
            }
          }
          else
          {
            ///System.Windows.Forms.MessageBox.Show("Could not fetch image = " + galleryItem);
          }
        }
        progress.NoticeProcessingIncrease(1);

        if (progress.IsStopRequested)
          return;
      }

      // first call: http://xxx.deviantart.com/gallery/?catpath=/&offset=24
      // offset goes from 0 - 24 - 48
      // this get deviations in form like: xxx.deviantart.com/art/title-is-here-666666666
      // <span class="details"><a href="http://xxx.deviantart.com/art/title-is-here-666666666?q=sort%3Atime%20gallery%3Axxx&amp;qo=24" class="t"

      // then we can extract id from this:
      // xxx.deviantart.com/art/title-is-here-666666666 -> 666666666
      // this id is used to compare if it was already downloaded!

      // then we open each not downloaded xxx.deviantart.com/art/title-is-here-666666666
      // and search for:http://www.deviantart.com/download/666666666/title is here by author.jpg?token=6bee6f8e6268261de00bc6d25cdc72ba397664a8&ts=1419856645
      // this should be in <a class="dev-page-button dev-page-button-with-text dev-page-download" as a href param!
      // if it is not found - no direct download possible -> then lets search for:
      // <img collect_rid="1:666666666" objects with class="dev-content-full" (should be download like item) or class="dev-content-normal" or any other
      // their src attr should provide us with desired item
    }
  }

  public class CookieAwareWebClient : WebClient
  {
    public CookieAwareWebClient()
    {
      CookieContainer = new CookieContainer();

      this.LastRequest = DateTime.MinValue;
      MinimalInterval = new TimeSpan(0);

      this.PersistentHeaders = new WebHeaderCollection();
    }
    public CookieContainer CookieContainer { get; private set; }

    public DateTime LastRequest { get; private set; }
    public TimeSpan MinimalInterval { get; set; }
    public WebHeaderCollection PersistentHeaders { get; private set; }

    protected override WebRequest GetWebRequest(Uri address)
    {
      DateTime now = DateTime.UtcNow;
      TimeSpan interval = now - LastRequest;
      System.Diagnostics.Debug.WriteLine("Interval time: " + interval);
      if (interval < MinimalInterval)
        System.Threading.Thread.Sleep(MinimalInterval - interval);

      LastRequest = DateTime.UtcNow;

      foreach (string headerKey in this.PersistentHeaders.Keys)
        this.Headers[headerKey] = this.PersistentHeaders[headerKey];

      HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
      request.CookieContainer = CookieContainer;
      //request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0";

      return request;
    }
  }
}
