﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Runaurufu.GalleryDownloader.Base;
using Runaurufu.Utility.Reflection;

namespace Runaurufu.GalleryDownloader.Plugins
{
  /// <summary>
  /// Interaction logic for DevianArtSettings.xaml
  /// </summary>
  public partial class DevianArtSettings : UserControl, IProviderSettingsControl
  {
    public DevianArtSettings()
    {
      InitializeComponent();
    }

    public IProviderDefinition Provider
    {
      get;
      set;
    }

    public void SetSettingsToControl()
    {
      DevianArtProvider dap = this.Provider as DevianArtProvider;
      if (dap == null)
        return;

      this.userName.TextChanged -= control_TextChanged;
      this.userPassword.TextChanged -= control_TextChanged;

      this.userName.Text = dap.AuthUserName;
      this.userPassword.Text = dap.AuthUserPassword;

      this.userName.TextChanged += control_TextChanged;
      this.userPassword.TextChanged += control_TextChanged;
    }

    public void SetSettingsToProvider()
    {
      DevianArtProvider dap = this.Provider as DevianArtProvider;
      if (dap == null)
        return;

      dap.AuthUserName = this.userName.Text;
      dap.AuthUserPassword = this.userPassword.Text;
    }

    private void control_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.SetSettingsToProvider();
    }
  }
}
