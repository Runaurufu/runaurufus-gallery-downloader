﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.GalleryDownloader.Base
{
  public interface IRecordData
  {
    bool WasItemDownloaded(Gallery gallery, string ident);
    void RegisterDownloadedItem(Gallery gallery, string itemIdent);
  }
}
