﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.GalleryDownloader.Base
{
  public class Gallery
  {
    public Int64 Id { get; set; }
    public String Name { get; set; }
    public String Uri { get; set; }
    public String SavePath { get; set; }
  }
}
