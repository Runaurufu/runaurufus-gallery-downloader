﻿using System.Collections.Generic;

namespace Runaurufu.GalleryDownloader.Base
{
  public interface IProviderDefinition
  {
    /// <summary>
    /// name must be unique in aspect to all existing providers!
    /// </summary>
    string Name { get; }

    bool PreGalleryItemsDownload(IProgressHandling progress);

    void DownloadGalleryItems(IProgressHandling progress, IRecordData record, Gallery gallery);

    void PostGalleryItemsDownload(IProgressHandling progress);

    void SetSettings(Dictionary<string, string> settings);
    Dictionary<string, string> GetSettings();

    IProviderSettingsControl GetSettingsControl();
  }

  public interface IProviderSettingsControl
  {
    IProviderDefinition Provider { get; set; }
    void SetSettingsToControl();
    void SetSettingsToProvider();
  }
}
