﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.GalleryDownloader.Base
{
  public interface IProgressHandling
  {
    void SetProcessingSteps(int steps);
    void SetProcessingContinuous(string progressText);
    void NoticeProcessingIncrease(int progress = 1);
    void NoticeProcessingDecrease(int progress = 1);
    bool IsStopRequested { get; }
  }
}
